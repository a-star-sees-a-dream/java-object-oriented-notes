# 1、笔记

成员变量：在类中方法外的变量叫成员变量

局部变量：在类中方法中的变量叫局部变量

成员方法：类中的方法就叫成员方法

private关键字，表示私有化，与public（公有化）相对立，都是修饰符。被private修饰的成员（成员变量和成员方法）只能够在本类中才能访问。如果需要被其他类调用，需要编写相应的方法setXxx（赋值）和getXxx（取值），这两个方法都用public修饰

面向对象三大特征之一（**封装，继承，多态**)

```java
public void setAge(int age){// this.表示成员（成员变量和成员方法），需要有形参列表，不需要返回值
    this.age = age;
}
public int getAge(){
    return this.age;// this可以不写
}
```

this：代表所在类的对象引用，方法被哪个对象调用，this就代表哪个对象

toString:表示一字符串形式打印

```java
public String toString(){
    String str = "XXXXXX" + XXX;
    return str;
}

// 示例
//  类
public class Dog {
    String name;
    public String toString(){
        String str = "name: "+name;
        return str;
    }
}

//引用类
public class Mock {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.name = "mockQ";
        System.out.println(dog);
    }
}
输出 name = mockQ

```

构造方法的创建：

​		如果没有定义构造方法，系统将给出一个**默认**的**无参数构造方法**

​		如果定义了构造方法，系统将不再提供默认的构造方法

构造方法的重载：

​		如果自定义了带参构造方法，还要使用无参数构造方法，就必须再写一个无参数构造方法

无论是否使用，都手动书写无参数构造方法，和带参数构造方法





标准类的制作：

①成员变量 

使用**private**修饰

②构造方法 

提供一个无参构造方法

提供一个带多个参数的构造方法

③成员方法 

提供每一个成员变量对应的setXxx()/getXxx()

提供一个显示对象信息的**show()**（即toString）

④创建对象并为其成员变量赋值的两种方式

无参构造方法创建对象后使用**setXxx**(）赋值

使用带参构造方法直接创建带有属性值的对象

![image-20230407173437631](E:\Java\20230410 面向对象\类和对象 作业\image-20230407173437631.png)

题目：

# 2、作业

1，创建一个员工类（标准类）

```java
public class Employee {
    private int id;
    public void setId(int id){
        if (id < 0 ){
            System.out.println("错误的编号");
            return;
        }
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    private String name;
    public void setName(String name){
            this.name = name;
    }

    public String getName(){
        return this.name;
    }
    private int age;
    public void setAge(int age){
        if (age <= 0){
            System.out.println("您活在地府？");
            return;
        } else if (age > 120) {
            System.out.println("您长生不死？");
        } else {
            this.age = age;
        }
    }
    public int getAge(){
        return this.age;
    }
    private double salary;
    public void setSalary(double salary){
        if (salary <= 2000 ){
            System.out.println("您看看路边的杆子上，少了点啥");
            return;
        }else {
            this.salary = salary;
        }
    }
    public double getSalary(){
        return this.salary;
    }
    public Employee(){

    }
    public Employee(int id,String name,int age,double salary){
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    public String toString(){
        String str = "编号："+id+" 姓名:"+name+" 年龄:"+age+" 薪资:"+salary;
        return str;
    }
}

```

2.在测试类中用无参和有参两种构造器。分别创建一个对象。并打印对象

```java
public class Test01 {
    public static void main(String[] args) {
        Employee e1 = new Employee();
        e1.setId(1);
        e1.setName("小湫");
        e1.setAge(26);
        e1.setSalary(8050.40);
        System.out.println("第1个员工的" + e1);
        Employee e2 = new Employee(2, "湫酱", 24, 21000.0);
        System.out.println("第2个员工的" + e2);
    }
}

```

